﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using MessageService.Interfaces;
using MessageService.Model;
using MessageService.Requests;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MessageService.Controllers
{
    [Route("api/[controller]")]
    public class ContactsController : Controller
    {
        private IContactRepository repository;

        public ContactsController(IContactRepository repository)
        {
            this.repository = repository;
        }

        // GET api/contacts
        /// <summary>
        /// Get all contacts
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                return Ok(repository.GetAllContacts());
            }
            catch (Exception)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
            
        }


        // GET api/contacts/5
        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            if (id < 1)
            {
                return NotFound();
            }
            else
            {
                return Ok(repository.GetContact(id));

            }
        }

        // POST api/contacts
        [HttpPost]
        public ActionResult Post([FromBody]CreateContactRequest value)
        {
            var createdContact = repository.CreateContact(value);
            if (createdContact != null && createdContact.ContactId > 0)
            {
                return Created(Request.Path.ToString() + "/" + createdContact.ContactId, createdContact);
            }
            else
            {
                return NotFound();
            }
        }

        // PUT api/contacts/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody]ContactModel value)
        {
            if (id < 1)
            {
                return BadRequest("ContactId should be a positive number");
            }
            else
            {
                return Ok(repository.ModifyContact(value));
            }
            
        }

        // DELETE api/contacts/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var isRemoved= repository.RemoveContact(id);
            if (isRemoved)
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
            
        }
    }
}

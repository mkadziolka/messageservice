﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MessageService.Interfaces;
using MessageService.Model;
using MessageService.Requests;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MessageService.Controllers
{
    [Route("api/[controller]")]
    public class MessagesController : Controller
    {
        private IMessageRepository repository;
       
        public MessagesController(IMessageRepository repository)
        {
            this.repository = repository;
        }


        // GET api/messages/search/5
        [HttpGet("Search/{id}")]
        public ICollection<MessageModel> Get(int id)
        {
            return repository.GetMessagesForUser(id);
        }

        // POST api/messages/search/5
        [HttpPost("Search/{id}")]
        public ICollection<MessageModel> Post(int id, [FromBody]SearchMessagesRequest rq)
        {
            return repository.SearchMessagesForUser(id, rq);
        }

        // POST api/messages/5
        [HttpPost("{id}")]
        public bool Post(int id, [FromBody]NewMessageRequest rq)
        {
             return repository.SendNewMessage(id, rq);
        }

       //TODO:Removing messages
    }
}

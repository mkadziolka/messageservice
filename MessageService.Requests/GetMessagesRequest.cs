﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessageService.Requests
{
    public class SearchMessagesRequest
    {
        public string Text { get; set; }
        public int SenderId { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public string SortOrder { get; set; }
    }
}

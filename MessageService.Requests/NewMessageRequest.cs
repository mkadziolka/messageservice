﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessageService.Requests
{
    public class NewMessageRequest
    {
        public int ReceiverId { get; set; }
        public string Text { get; set; }
    }
}

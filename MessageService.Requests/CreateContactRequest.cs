﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessageService.Requests
{
    public class CreateContactRequest
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}

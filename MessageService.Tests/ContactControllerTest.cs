using MessageService.Controllers;
using MessageService.Interfaces;
using MessageService.Model;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace MessageService.Tests
{
    public class ContactControllerTest
    {
        public ContactControllerTest()
        {

        }
        [Fact]
        public void GetAllContacts_Empty()
        {
            //arrange
            var repo = new Mock<IContactRepository>();
            repo.Setup(x => x.GetAllContacts()).Returns(new List<ContactModel>());
            var controller = new ContactsController(repo.Object);

            //act
            var actionResult = controller.Get();

            //assert
            Assert.IsType<OkObjectResult>(actionResult);
            var result = actionResult as OkObjectResult;
            Assert.NotNull(result);
            Assert.Empty(result.Value as IEnumerable<ContactModel>);
        }

        [Fact]
        public void GetAllContacts_NotEmpty()
        {
            //arrange
            var repo = new Mock<IContactRepository>();
            repo.Setup(x => x.GetAllContacts()).Returns(new List<ContactModel>() { new ContactModel(), new ContactModel() });
            var controller = new ContactsController(repo.Object);

            //act
            var actionResult = controller.Get();

            //assert
            Assert.IsType<OkObjectResult>(actionResult);
            var result = actionResult as OkObjectResult;
            Assert.NotNull(result);
            Assert.NotNull(result.Value);
            Assert.True(result.Value is IEnumerable<ContactModel>);
            var list = result.Value as List<ContactModel>;
            Assert.NotEmpty(list);
        }

        [Fact]
        public void GetAllContacts_Error()
        {
            //arrange
            var repo = new Mock<IContactRepository>();
            repo.Setup(x => x.GetAllContacts()).Throws(new Exception());
            var controller = new ContactsController(repo.Object);

            //act
            var actionResult = controller.Get();

            //assert
            Assert.IsType<StatusCodeResult>(actionResult);
            var result = actionResult as StatusCodeResult;
            Assert.NotNull(result);
            Assert.Equal(500, result.StatusCode);
        }

        [InlineData(-1)]
        [InlineData(0)]
        [Theory]
        public void GetContact_NegativeId(int id)
        {
            //arrange
            var repo = new Mock<IContactRepository>();
            var controller = new ContactsController(repo.Object);

            //act
            var actionResult = controller.Get(id);

            //assert
            Assert.IsType<NotFoundResult>(actionResult);
            var result = actionResult as NotFoundResult;
            Assert.NotNull(result);
            Assert.Equal(404, result.StatusCode);
        }

      
        [InlineData(1)]
        [Theory]
        public void GetContact_PositiveId_ContactFound(int id)
        {
            //arrange
            var repo = new Mock<IContactRepository>();
            repo.Setup(x => x.GetContact(id)).Returns(new ContactModel());
            var controller = new ContactsController(repo.Object);

            //act
            var actionResult = controller.Get(id);

            //assert
            Assert.IsType<OkObjectResult>(actionResult);
            var result = actionResult as OkObjectResult;
            Assert.NotNull(result.Value);
            Assert.Equal(200, result.StatusCode);
        }

        [InlineData(1)]
        [Theory]
        public void GetContact_PositiveId_ContactNotFound(int id)
        {
            //arrange
            var repo = new Mock<IContactRepository>();
            repo.Setup(x => x.GetContact(id)).Returns<ContactModel>(null);
            var controller = new ContactsController(repo.Object);

            //act
            var actionResult = controller.Get(id);

            //assert
            Assert.IsType<OkObjectResult>(actionResult);
            var result = actionResult as OkObjectResult;
            Assert.Null(result.Value);
            //Browser actually recieves 204, due to NoContentFormatter executed after returning from controller.
            Assert.Equal(200, result.StatusCode);
        }
    }
}

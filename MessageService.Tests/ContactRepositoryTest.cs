﻿using MessageService.Model;
using MessageService.Repository;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MessageService.Tests
{
    public class ContactRepositoryTest
    {
        [Fact]
        public void CreateNewContact()
        {
            var options = new DbContextOptionsBuilder<MessageServiceContext>()
                .UseInMemoryDatabase(databaseName: "TestDb")
                .Options;
            using (var db = new MessageServiceContext(options))
            {
                var repo = new ContactRepository(db);
                var rq = new Requests.CreateContactRequest
                {
                    Email = "mail@gmail.com",
                    Name = "John"
                };
                var responseModel = repo.CreateContact(rq);

                Assert.NotNull(responseModel);
                Assert.Equal(rq.Name, responseModel.Name);
                Assert.Equal(rq.Email, responseModel.Email);
                Assert.True(responseModel.ContactId > 0, "ContactId is not greater than zero");
            }
        }

        [Fact]
        public void RemoveExistingContact()
        {
            var options = new DbContextOptionsBuilder<MessageServiceContext>()
                .UseInMemoryDatabase(databaseName: "TestDb")
                .Options;
            using (var db = new MessageServiceContext(options))
            {
                var repo = new ContactRepository(db);
                var rq = new Requests.CreateContactRequest
                {
                    Email = "mail@gmail.com",
                    Name = "John"
                };
                var responseModel = repo.CreateContact(rq);
                var isRemoved = repo.RemoveContact(responseModel.ContactId);
                var removedContact = repo.GetContact(responseModel.ContactId);

                Assert.True(isRemoved);
                Assert.Null(removedContact);
                
            }
        }

        [Fact]
        public void RemoveNonExistingContact()
        {
            var options = new DbContextOptionsBuilder<MessageServiceContext>()
                .UseInMemoryDatabase(databaseName: "TestDb")
                .Options;
            using (var db = new MessageServiceContext(options))
            {
                var repo = new ContactRepository(db);
               
                var isRemoved = repo.RemoveContact(-1);

                Assert.False(isRemoved);
               }
        }
    }
}

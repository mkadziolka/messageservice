﻿using MessageService.Interfaces;
using MessageService.Model;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MessageService.Requests;

namespace MessageService.Repository
{
    public class ContactRepository : IContactRepository
    {
        MessageServiceContext db;
        public ContactRepository(MessageServiceContext db)
        {
            this.db = db;
        }
        public IEnumerable<ContactModel> GetAllContacts()
        {
            return db.Contacts.ToList();
        }

        public ContactModel GetContact(int id)
        {
            return db.Contacts.FirstOrDefault(x => x.ContactId == id);
        }

        public ContactModel CreateContact(CreateContactRequest request)
        {
            var model = new ContactModel
            {
                Name = request.Name,
                Email = request.Email
            };
            db.Contacts.Add(model);
            db.SaveChanges();
            return model;
        }

        public bool ModifyContact(ContactModel model)
        {
            db.Contacts.Update(model);
            db.SaveChanges();
            return true;
        }

        public bool RemoveContact(int id)
        {
            var model = db.Contacts.FirstOrDefault(x => x.ContactId == id);
            if ( model== null)
            {
                return false;
            }
            db.Contacts.Remove(model);
            db.SaveChanges();
            return true;
        }
    }
}

﻿using MessageService.Interfaces;
using MessageService.Model;
using MessageService.Requests;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MessageService.Repository
{
    public class MessageRepository : IMessageRepository
    {
        MessageServiceContext db;
        public MessageRepository(MessageServiceContext db)
        {
            this.db = db;
        }

        public ICollection<MessageModel> GetMessagesForUser(int receiverId)
        {
            return db.Messages.Include(x => x.Sender).Where(x => x.Receiver.ContactId == receiverId).ToList();
        }

        public ICollection<MessageModel> SearchMessagesForUser(int receiverId, SearchMessagesRequest rq)
        {
            var messages = db.Messages.Where(x => x.Receiver.ContactId == receiverId);
            if (rq.SenderId != 0)
            {
                messages = messages.Where(x => x.SenderContactId == rq.SenderId);
            }
            if (!string.IsNullOrEmpty(rq.Text))
            {
                messages = messages.Where(x => x.Text.Contains(rq.Text));
            }
            if(rq.SortOrder == "asc")
            {
                messages = messages.OrderBy(x => x.Sent);
            }
            if (rq.SortOrder == "desc")
            {
                messages = messages.OrderByDescending(x => x.Sent);
            }
            if (rq.PageNumber>0 && rq.PageSize > 0)
            {
                messages = messages.Skip(rq.PageSize*(rq.PageNumber-1)).Take(rq.PageSize);
            }
            return messages.ToList();
        }

        public bool SendNewMessage(int id, NewMessageRequest rq)
        {
            var message = new MessageModel
            {
                ReceiverContactId = rq.ReceiverId,
                SenderContactId = id,
                Text = rq.Text,
                Sent = DateTime.Now
            };
            db.Messages.Add(message);
            db.SaveChanges();
            return true;
        }
    }
}

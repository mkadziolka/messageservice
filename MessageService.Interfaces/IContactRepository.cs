﻿using MessageService.Model;
using MessageService.Requests;
using System;
using System.Collections.Generic;

namespace MessageService.Interfaces
{
    public interface IContactRepository
    {
        IEnumerable<ContactModel> GetAllContacts();
        ContactModel GetContact(int id);
        ContactModel CreateContact(CreateContactRequest request);
        bool ModifyContact(ContactModel model);
        bool RemoveContact(int id);
    }
}

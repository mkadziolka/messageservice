﻿using MessageService.Model;
using MessageService.Requests;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessageService.Interfaces
{
   public  interface IMessageRepository
    {
        ICollection<MessageModel> GetMessagesForUser(int receiverId);
        ICollection<MessageModel> SearchMessagesForUser(int receiverId, SearchMessagesRequest rq);
        bool SendNewMessage(int id, NewMessageRequest rq);
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MessageService.Model
{
    [Table("Contacts")]
    public class ContactModel
    {
        [Key]
        public int ContactId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public virtual ICollection<ContactModel> SentMessages { get; set; }
        public virtual ICollection<ContactModel> ReceivedMessages { get; set; }
    }
}

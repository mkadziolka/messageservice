﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MessageService.Model
{
    [Table("Messages")]
    public class MessageModel
    {
        [Key]
        public long MesssageId { get; set; }
        public DateTime Sent { get; set; }
        public string Text { get; set; }
        [ForeignKey("ContactId")]
        public int? SenderContactId { get; set; }
        [ForeignKey("ContactId")]
        public int? ReceiverContactId { get; set; }

        public virtual ContactModel Sender { get; set; }
        public virtual ContactModel Receiver { get; set; }
    }
}

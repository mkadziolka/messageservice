﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
//using System.Data.Entity;
using System.Text;


namespace MessageService.Model
{
    public class MessageServiceContext : DbContext
    {
        public MessageServiceContext(DbContextOptions<MessageServiceContext> options) : base(options)
        {
        }

        public DbSet<ContactModel> Contacts { get; set; }
        public DbSet<MessageModel> Messages { get; set; }

    }
}

